# GitLab Archiver

Locally archive all projects from your GitLab account!

## Requirements
* Software:
  * Git
  * Python 3
* Python Packages:
  * `GitPython`
  * `python_gitlab`

## Setup

* Clone this repository to your PC
  * `git clone https://gitlab.com/Blazin64/gitlab-archiver`
* Enter the repository
  * `cd gitlab-archiver`
* Install the required Python packages
  * `pip3 install -r requirements.txt`
* Enable GitLab API acccess
  * Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with API permissions.
  * Open `gitlab-archiver.py` in an editor and set the token as the value of `GITLAB_TOKEN`.
* Enable GitLab SSH access
  * You have likely already done this, if you have ever pushed commits to GitLab.
  * See [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html)

## Usage

### Preparation

GitLab Archiver has to be run inside a Git repository, since it stores your GitLab projects as submodules. It is recommended to make one the usual way - by creating an empty directory and running `git init` inside it.

### Begin Archiving

Open a terminal in your empty Git repository. Run `/path/to/gitlab-archiver.py`, and the script will begin archiving your GitLab projects.
