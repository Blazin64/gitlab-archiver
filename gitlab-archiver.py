#!/usr/bin/env python3

'''
    GitLab Archiver
    Copyright (C) 2022  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import git
import gitlab

# Put your GitLab API token here!
GITLAB_TOKEN = ''

# Prints git submodule entries based on a project list
def add_modules(projects, repo, username):
    modules = ""

    for x in projects:
        url = f"git@gitlab.com:{username}/{x.path}.git"
        print(f"Adding as submodule: {url}")
        try:
            git.Submodule.add(repo, x.path, x.path, url)
        except:
            print(f"    Error adding submodule.")

    print(modules)

# Verify that the current directory is inside a git repository
try:
    repo = git.Repo('.', search_parent_directories=True)
except:
    exit("The current directory is not part of a git repository!")

# Attempt authorization with the API token
try:
    gl = gitlab.Gitlab(private_token = GITLAB_TOKEN)
    gl.auth()
except:
    exit("Authorization failed. Is your token valid?")

# Get all projects on the GitLab account
projects = gl.projects.list(owned=True)

# Print a git submodule entry for each project
if len(projects) > 0:
    add_modules(projects, repo, gl.user.username)
else:
    exit("Your account has no projects...")
